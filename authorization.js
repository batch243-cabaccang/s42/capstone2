require("dotenv").config();

const jwt = require("jsonwebtoken");
const forToken = "oi3987omcvm09u230923987ginycg98c4mwliumc092x3om8igkj";

module.exports.authToken = (user) => {
    return jwt.sign(user.toJSON(), forToken);
};

module.exports.verifyToken = (req, res, next) => {
    try {
        const token = req.headers.authorization;
        if (!token) return res.send("No Token");
        const tokenReceived = token.split(" ")[1];

        jwt.verify(tokenReceived, forToken);
        next();
    } catch (error) {
        res.send({ message: error.message });
    }
};

module.exports.decodeToken = (token) => {
    try {
        if (!token) return null;
        token = token.split(" ")[1];

        return jwt.decode(token, { complete: true }).payload;
    } catch (error) {
        res.send({ message: error.message });
    }
};
