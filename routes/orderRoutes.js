const express = require("express");
const orderRoutes = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../authorization");

orderRoutes.post("/received/:orderId", auth.verifyToken, orderController.orderRecieved);

orderRoutes.get("/history", auth.verifyToken, orderController.orderHistory);

orderRoutes.post("/history/filtered", auth.verifyToken, orderController.filteredHistory);

module.exports = orderRoutes;
