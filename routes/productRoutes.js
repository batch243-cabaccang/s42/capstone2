const express = require("express");
const productRoutes = express.Router();
const productController = require("../controllers/productController");
const auth = require("../authorization");

// ADMIN ONLY ----------------------------------------------------
// Create Product
productRoutes.post("/createProduct", auth.verifyToken, productController.createProduct);
// Create Bundle Product
productRoutes.post("/createBundle", auth.verifyToken, productController.createBundleProduct);

// Get/View All Products
productRoutes.get("/allProducts", auth.verifyToken, productController.getAllProducts);

// Find One Product
productRoutes.get("/findProduct/:productId", auth.verifyToken, productController.findProduct);

// Update Product
productRoutes.patch("/updateProduct/:productId", auth.verifyToken, productController.updateProduct);

// Archive or Unarchive Product
productRoutes.patch(
    "/archiveProduct/:productId",
    auth.verifyToken,
    productController.archiveOrUnarchiveProduct
);
// End of ADMIN ONLY------------------------------------------------

// GET Availalbe Products
productRoutes.get("/:productType/availableProducts", productController.getAvailableProdcuts);

// ADD to or Remove from CART (bundle product)
productRoutes.patch("/:actionType", auth.verifyToken, productController.addOrRemoveItems);

// Get products with filter
productRoutes.get("/:filter", productController.getFilteredProducts);

module.exports = productRoutes;
