const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
    {
        orderedBy: { type: mongoose.Types.ObjectId, ref: "User" },
        productOrdered: { type: String, required: true },
        order: { type: Object, default: {} },
        totalAmoutToBePaid: { type: Number, requied: true },
        paymentMethod: { type: String, required: true },
        isPaid: { type: Boolean, default: false },
        status: { type: String, default: "Order Pending" },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Order", orderSchema);
