const mongoose = require("mongoose");

const typeNumber = { type: Number, required: true, default: 0 };

const bundleProductSchema = new mongoose.Schema(
    {
        bundledBy: { type: Object, required: true },
        bundleProductImages: { type: Array, default: [] },
        bundleName: { type: String, required: true },
        bundledItems: [{ type: mongoose.Types.ObjectId, ref: "Product" }],
        originalPrice: typeNumber,
        discount: { type: String, default: "10%" },
        totalPrice: typeNumber,
        stock: typeNumber,
        totalBuyCount: typeNumber,
        isAvailable: { type: Boolean, default: true },
        hasDiscount: { type: Boolean, default: true },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Bundle", bundleProductSchema);
