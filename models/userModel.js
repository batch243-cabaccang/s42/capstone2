const mongoose = require("mongoose");

const typeString = { type: String, required: true };
const typeNumber = { type: Number, required: true };

const userSchema = new mongoose.Schema(
    {
        email: typeString,
        password: { type: Object, required: true },
        firstName: typeString,
        lastName: typeString,
        mobileNum: typeNumber,
        address: [
            {
                streetNumberAndName: typeString,
                brgyDistrict: typeString,
                cityMunicipality: typeString,
                country: typeString,
                postalCode: typeNumber,
            },
        ],
        paymentMethods: [{ type: Object }],
        isAdmin: {
            type: Boolean,
            default: false,
        },
        cart: { type: mongoose.Types.ObjectId, ref: "Cart" },
        postedProducts: [{ type: mongoose.Types.ObjectId, ref: "Product" }],
        orders: [{ type: mongoose.Types.ObjectId, ref: "Order" }],
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("User", userSchema);
