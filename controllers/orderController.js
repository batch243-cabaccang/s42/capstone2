const User = require("../models/userModel");
const Product = require("../models/productModel");
const Cart = require("../models/cartModel");
const Order = require("../models/orderModel");
const auth = require("../authorization");

module.exports.orderRecieved = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    if (userData.isAdmin) return res.send("Admins can't do this.");

    const orderId = req.params.orderId;

    try {
        const user = await User.findById(userData._id);
        const userOrder = await Order.findById(orderId);

        if (userOrder.orderedBy != user.id) return res.json({ message: "This is not your order" });

        if (userOrder.status == "Received") return res.json({ message: "Order Already Received" });

        userOrder.status = "Received";
        await userOrder.save();
        res.json({ message: "Order Received" });
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.orderHistory = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);

    try {
        const userOrders = await Order.find({ orderedBy: userData._id });
        const reverseOrderOfOrders = userOrders.reverse();
        res.json(reverseOrderOfOrders);
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.filteredHistory = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decodeToken(token);
    const filterDetails = req.body;

    let value = { $regex: filterDetails.value, $options: "i" };
    if (filterDetails.filter === "isPaid") {
        value = filterDetails.value;
    }

    try {
        const filteredOrders = await Order.find({
            orderedBy: userData._id,
            [filterDetails.filter]: value,
        });
        const filteredOrdersReveresed = filteredOrders.reverse();
        res.json(filteredOrdersReveresed);
    } catch (error) {
        res.json({ message: error.message });
    }
};
